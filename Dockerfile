# Start from git because we need to fetch modified files.
# Older version or python 3.9 breaks.
FROM alpine/git:v2.34.2

# Install python3 xlmlint so we can run the entvalidate script.
RUN apk add python3~=3.9 libxml2-utils

# Required for mod IO upload & signing.
RUN apk add minisign && python3 -m ensurepip && pip3 install --no-cache-dir requests

ENTRYPOINT ["sh"]
